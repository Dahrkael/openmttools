# Open MT Tools

open source tools to initialize MediaTek chips and their drivers on non-Android Linux.

a reimplementation of the userspace binaries that interface with the WMT drivers:
* mtinit replaces wmt_loader
* mtdaemon replaces connsys_launcher/6620_launcher

This is a work-in-progress! 
* mtinit does not load driver modules into the kernel. do it manually before executing it if needed.
* mtdaemon implements the BTIF mode. UART mode is missing.

Further testing is required to make sure it works with every MTK chip. Feel free to try them with yours and open an issue if something is wrong.
